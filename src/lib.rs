#[cfg(windows)]
extern crate winapi;
#[cfg(windows)]
pub mod api {
    use winapi::um::winuser as user32;
    use winapi::um::processthreadsapi as procapi;
    use std::ffi::CString;
    use winapi::ctypes::c_void;
    use winapi::shared::windef::{HWND, POINT};
    use winapi::shared::minwindef::{LPARAM, DWORD};
    use winapi::shared::windef::RECT;
    use winapi::um::winnt::{THREAD_QUERY_LIMITED_INFORMATION, PROCESS_QUERY_INFORMATION, PROCESS_VM_READ};
    use winapi::um::winuser::{SWP_NOMOVE, SWP_NOSIZE, HWND_TOPMOST, HWND_NOTOPMOST, SW_SHOWNORMAL, GWL_EXSTYLE, WS_EX_LAYERED, WS_EX_NOACTIVATE, WS_EX_TRANSPARENT, GetDC, ULW_COLORKEY, LWA_COLORKEY, SW_RESTORE, SW_MAXIMIZE, SPI_GETWORKAREA, HWND_TOP, SWP_NOACTIVATE, WS_EX_TOOLWINDOW, WS_EX_APPWINDOW, SW_SHOW, SW_HIDE};
    use winapi::um::psapi;
    use winapi::um::wingdi::CreateCompatibleDC;

    //use winapi::shared::ntdef::NULL;

    #[derive(Clone)]
    pub struct Window {
        handle: HWND,
    }

    impl Window {

        ///
        /// Get the work area size and location of the primary monitor (size without taskbar),
        /// then size the window to fill it.
        ///
        pub fn fill_workarea(&self) {
            unsafe {
                let mut rect = RECT {
                    bottom: 0,
                    left: 0,
                    right: 0,
                    top: 0,
                };

                if user32::SystemParametersInfoA(SPI_GETWORKAREA, 0, &mut rect as *mut _ as *mut c_void, 0) != 0 {
                    user32::SetWindowPos(self.handle, HWND_TOP, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_NOACTIVATE);
                }
            }
        }

        ///
        /// Unminimizes the window if minimized.
        ///
        pub fn unminimize(&self) {
            unsafe {
                if user32::IsIconic(self.handle) != 0 {
                    user32::ShowWindow(self.handle, SW_SHOWNORMAL);
                }
            }
        }

        ///
        /// Maximizes the window if not minimized
        ///
        pub fn maximize(&self) {
            unsafe {
                if user32::IsIconic(self.handle) == 0 {
                    user32::ShowWindow(self.handle, SW_MAXIMIZE);
                }
            }
        }

        ///
        /// Hide the window
        ///
        pub fn hide_window(&self) {
            unsafe {
                user32::ShowWindow(self.handle, SW_HIDE);
            }
        }

        ///
        /// Show the window
        ///
        pub fn show_window(&self) {
            unsafe {
                user32::ShowWindow(self.handle, SW_SHOW);
            }
        }

        ///
        /// Unmaximizes the window if not minimized
        ///
        pub fn unmaximize(&self) {
            unsafe {
                if user32::IsIconic(self.handle) == 0 {
                    user32::ShowWindow(self.handle, SW_RESTORE);
                }
            }
        }

        ///
        /// Brings the window to the front of the window stack.
        ///
        /* There's a lot of silly things going on here.
         *
         * SetForegroundWindow does not allow "focus stealing" unless the current active window
         * allows it. Since we absolutely want our window displayed (Focus or not in this case
         * [for now]), we can work around this by setting our window here as a topmost window, to
         * bring it into view, and then unsetting that so it acts like a normal window again.
         *
         * NOTE: This does not focus the window! The window cannot be focused.
         */
        pub fn show(&self) {
            unsafe {
                user32::SetWindowPos(self.handle, HWND_TOPMOST,
                                     0, 0, 0, 0,
                                     SWP_NOMOVE | SWP_NOSIZE);
                user32::SetWindowPos(self.handle, HWND_NOTOPMOST,
                                     0, 0, 0, 0,
                                     SWP_NOMOVE | SWP_NOSIZE);
            }
        }

        ///
        /// Makes the window topmost or not
        ///
        pub fn topmost(&self, on: bool) {
            unsafe {
                if on {
                    user32::SetWindowPos(self.handle, HWND_TOPMOST,
                                         0, 0, 0, 0,
                                         SWP_NOMOVE | SWP_NOSIZE);
                } else {
                    user32::SetWindowPos(self.handle, HWND_NOTOPMOST,
                                         0, 0, 0, 0,
                                         SWP_NOMOVE | SWP_NOSIZE);
                }
            }
        }

        unsafe fn flip_ext_style_flag(&self, style: DWORD) {
            let style = style as u32 as i32;
            let cur = user32::GetWindowLongA(self.handle, GWL_EXSTYLE) as i32;
            if user32::SetWindowLongA(self.handle, GWL_EXSTYLE, cur ^ style) == 0 {
                panic!("Couldn't set window long A");
            }
        }

        ///
        /// Toggle appwindow on or off.
        ///
        pub fn appwindow(&self) {
            unsafe {
                self.flip_ext_style_flag(WS_EX_APPWINDOW);
            }
        }

        ///
        /// Toggle toolwindow on or off.
        ///
        pub fn toolwindow(&self) {
            unsafe {
                self.flip_ext_style_flag(WS_EX_TOOLWINDOW);
            }
        }

        ///
        /// Toggle layered on or off.
        ///
        pub fn layered(&self) {
            unsafe {
                self.flip_ext_style_flag(WS_EX_LAYERED);
            }
        }

        ///
        /// Toggle no_activate on or off.
        ///
        pub fn no_activate(&self) {
            unsafe {
                self.flip_ext_style_flag(WS_EX_NOACTIVATE);
            }
        }

        ///
        /// Toggle transparent on or off.
        ///
        pub fn transparent(&self) {
            unsafe {
                self.flip_ext_style_flag(WS_EX_TRANSPARENT);
            }
        }

        ///
        /// Attempts to focus the window.
        /// Returns if it succeeded.
        ///
        pub fn focus(&self) -> bool {
            unsafe {
                self.unminimize();
                user32::SetForegroundWindow(self.handle) > 0
            }
        }

        pub fn make_color_transparent(&self, rgb: (u8, u8, u8)) {
            unsafe {
                /*let dc = GetDC(std::ptr::null_mut());
                let mem = CreateCompatibleDC(dc);
                let mut p = POINT {
                    x: 0, y: 0
                };*/
                let color = rgb.0 as u32 & ((rgb.1 as u32) << 2) & ((rgb.2 as u32) << 4);
                //user32::UpdateLayeredWindow(self.handle, dc, std::ptr::null_mut(), std::ptr::null_mut(), mem, &mut p, color, std::ptr::null_mut(), ULW_COLORKEY);
                user32::SetLayeredWindowAttributes(self.handle, color, 0, LWA_COLORKEY);
            }
        }

        ///
        /// Gets the base name of the window. This is the underlying name, not the title.
        /// Returns the base name of the window.
        ///
        pub fn get_base_name(&self) -> String {
            unsafe {
                let mut alloc = Vec::new();
                alloc.resize(99,1);
                // The resize gives us enough string space in memory to hold the title.
                // I don't really know a better way, but there probably is one.

                let cret: *mut i8 = CString::from_vec_unchecked(alloc).into_raw() as *mut i8;
                //user32::GetWindowModuleFileNameA(self.handle, ret, 64);
                let proc = procapi::OpenProcess(
                                PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
                                0, self.get_pid() as u32);
                let mut len = psapi::GetModuleBaseNameA(proc, std::ptr::null_mut(), cret, 99);
                let ret8 = String::from_utf8_lossy(
                    std::slice::from_raw_parts_mut(cret as *mut u8, len as usize)).to_string();

                if ret8.contains(std::char::REPLACEMENT_CHARACTER) {
                    let mut alloc16 = Vec::new();
                    alloc16.resize(200 as usize,1); // Double size for double char size
                    let cret16: *mut u16 = CString::from_vec_unchecked(alloc16).into_raw() as *mut u16;

                    len = psapi::GetModuleBaseNameW(proc,std::ptr::null_mut(), cret16, 99);
                    return String::from_utf16_lossy(
                        std::slice::from_raw_parts_mut(cret16, len as usize)).to_string();
                }

                return ret8;
            }
        }

        // This skips over the thread ID and goes straight for the process ID. Not really sure which
        // one to return. XDO only seems to have a pid? So consistency I guess.
        ///
        /// Returns the process ID of the window.
        ///
        pub fn get_pid(&self) -> u64 {
            let ret: u64;
            unsafe {
                let thread_id = user32::GetWindowThreadProcessId(
                    self.handle, std::ptr::null_mut() );
                let thread = procapi::OpenThread(
                    THREAD_QUERY_LIMITED_INFORMATION, 0, thread_id);
                ret = procapi::GetProcessIdOfThread(thread) as u64;

                if ret == 0 {
                    panic!("Could not find process ID!");
                }
                ret
            }
        }

        ///
        /// Returns the title of the window as displayed on the frame.
        ///
        pub fn get_title(&self) -> String {
            unsafe {
                if !self.handle.is_null() {
                    let len = user32::GetWindowTextLengthA(self.handle);
                    if len > 0 {
                        let mut alloc = Vec::new();
                        alloc.resize((len+1) as usize,1);
                        // The resize gives us enough string space in memory to hold the title.
                        // I don't really know a better way, but there probably is one.

                        let cret: *mut i8 = CString::from_vec_unchecked(alloc).into_raw() as *mut i8;

                        user32::GetWindowTextA(self.handle, cret, len+1);

                        let ret8 = String::from_utf8_lossy(
                            std::slice::from_raw_parts_mut(cret as *mut u8, len as usize)).to_string();

                        // Seems to ONLY work when there's unicode in the title?
                        if ret8.contains(std::char::REPLACEMENT_CHARACTER) {
                            let mut alloc16 = Vec::new();
                            alloc16.resize(((len+1)*2) as usize,1); // Double size for double char size
                            let cret16: *mut u16 = CString::from_vec_unchecked(alloc16).into_raw() as *mut u16;
                            user32::GetWindowTextW(self.handle, cret16, len+1);
                            return String::from_utf16_lossy(
                                std::slice::from_raw_parts_mut(cret16, len as usize)).to_string();
                        }

                        return ret8;

                    } else {
                        panic!("Invalid window handle! {:?}", self.handle);
                    }
                } else {
                    panic!("Window handle is null!");
                }
            }
        }

        ///
        /// Returns the window handle.
        ///
        pub fn get_handle(&self) -> *mut u64 {
            self.handle as *mut u64
        }

        ///
        /// Creates a window from a known handle.
        /// Returns the created window object.
        ///
        pub fn from_handle(hndl: *mut u64) -> Window {
            Window {
                handle: hndl as HWND,
            }
        }

    }

    ///
    /// Returns a window object for the currently focused window.
    ///
    pub fn get_current_window() -> Window {
        unsafe {
            Window {
                handle: user32::GetForegroundWindow()
            }
        }
    }

    struct Search {
        search_wins: Vec<Window>,
        search_max: usize,
        search_only_visible: bool,
        search_title: &'static str,
        search_base: &'static str,
        search_case: bool
    }

    ///
    /// Checks if a window matches specific search details.
    /// Returns 1, if matched pushes the window into a list.
    ///
    unsafe extern "system" fn enum_search(hwnd: HWND, lparam: LPARAM) -> i32 {
        let search: &mut Search = &mut *(lparam as *mut Search);
        if (user32::IsWindowVisible(hwnd) <= 0 && search.search_only_visible) ||
            user32::GetWindowTextLengthA(hwnd) <= 0 {
            return 1;
        }
        if search.search_title.len() > 0 {
            let twin = Window { handle: hwnd };
            let title = twin.get_title();
            if (search.search_case && !title.contains(search.search_title)) ||
                (!search.search_case &&
                    !title.to_lowercase().contains(search.search_title.to_lowercase().as_str())) {
                return 1;
            }
        }
        if search.search_base.len() > 0 {
            let twin = Window { handle: hwnd };
            let title = twin.get_base_name();
            if (search.search_case && !title.contains(search.search_title)) ||
                (!search.search_case &&
                    !title.to_lowercase().contains(search.search_title.to_lowercase().as_str())) {
                return 1;
            }
        }
        if search.search_wins.len() < search.search_max {
            search.search_wins.push(Window { handle: hwnd });
        }
        return 1;
    }

    ///
    /// Iterates through all available windows for ones that match a set criteria.
    /// All matching windows are added to search_wins.
    /// Returns a list of matching windows.
    ///
    //noinspection RsTypeCheck [temp for IntelliJ]
    // TODO: More options? (Shell, etc)
    pub fn search_windows(max: usize, only_visible: bool, title_contains: &'static str,
                          base_name_contains: &'static str, case_sensitive: bool) -> Vec<Window> {
        unsafe {
            let mut search = Search {
                search_wins: Vec::with_capacity(max),
                search_max: max,
                search_only_visible: only_visible,
                search_title: title_contains,
                search_base: base_name_contains,
                search_case: case_sensitive
            };
            let search_ptr: *mut Search = &mut search;

            user32::EnumWindows(Some(enum_search), search_ptr as isize);

            search.search_wins
        }
    }

}

#[cfg(linux)]
extern crate libxdo_sys as xdo;
#[cfg(linux)]
pub mod api {

}

#[cfg(test)]
mod tests {
    use api;

    #[test]
    fn win_handles() {
        let win = api::get_current_window();
        let newin = api::Window::from_handle(win.get_handle());
        println!("Title: {}", newin.get_title());
        println!("HWND: {}", newin.get_handle() as u64);
    }

    #[test]
    fn win_search() {
        let wins: Vec<api::Window> = api::search_windows(50, true, "", "",false);
        println!("{}", wins.len());
        //println!("{:?}", wins.get(2).unwrap().get_handle());
        for win in wins {
            println!("Title: {}", win.get_title());
        }
    }

    #[test]
    fn proc_id() {
        let win = api::get_current_window();
        println!("PID: {}", win.get_pid());
    }

    #[test]
    fn focus() {
        let wins = api::search_windows(1, true, "Atom", "",true);
        let win = wins.first().unwrap();
        win.focus();
    }

    #[test]
    fn name() {
        let win = api::get_current_window();
        println!("FILE: {}", win.get_base_name());
    }
}
